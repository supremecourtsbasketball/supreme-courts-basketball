Conveniently located 5 miles from I-88, near the Fox Valley Mall (see map below)

4 High School courts with premium wood floors

Full Climate Control (Cooled in the Summer, Heated in the Winter)

24 collegiate grade ceiling mounted baskets

Large wall and ceiling mount scoreboards on each court.



Address: 888 S Frontenac St, Aurora, IL 60504, USA

Phone: 630-340-4645

Website: http://supremecourtsbasketball.com
